package br.ucsal.testequalidade20192.locadora;



import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyNoMoreInteractions;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Modelo;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocacaoBO.class, ClienteDAO.class,VeiculoDAO.class, LocacaoDAO.class})
public class LocacaoBOUnitarioTest {
	/**
	 * Alunos: Jean Lima , Gleicy Maria 
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */
	
	@Before 
	public void setUp() {
		mockStatic(ClienteDAO.class);
		mockStatic(VeiculoDAO.class);
		mockStatic(LocacaoDAO.class);
	}


	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		List<String> placas = new ArrayList<String>();
		placas.add("ABC123");
		

		String cpf ="02147856987";	
		Integer diasLocacao = 2;
		Cliente cliente = new Cliente(cpf, "Jean", "9595995");
		Veiculo veiculo = new Veiculo(placas.get(0),2020, new Modelo("Uno"), 150D);
		
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		veiculos.add(veiculo);
		
		Date hoje = new Date();
		Locacao locacao = new Locacao(cliente,veiculos,hoje,2);

		
		when(ClienteDAO.obterPorCpf(anyString())).thenReturn(cliente);
		when(VeiculoDAO.obterPorPlaca(anyString())).thenReturn(veiculo);
		whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		
		LocacaoBO.locarVeiculos(cpf, placas, hoje,diasLocacao);
		
		
		verifyStatic(ClienteDAO.class);
		ClienteDAO.obterPorCpf(anyString());
		verifyStatic(VeiculoDAO.class);
		VeiculoDAO.obterPorPlaca(anyString());
		verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao);
		verifyNoMoreInteractions(LocacaoBO.class);
		
		
	}
}
